import requests
import os


def download_gz(urls):
    # определим имя директории, которую создаём
    path = "archives"
    try:
        os.mkdir(path)
    except OSError:
        print("Создать директорию %s не удалось, или она уже существует" % path)
    else:
        print("Успешно создана директория %s " % path)

    list_urls = iter(urls)
    for url in list_urls:
        name = 'archives/{}'.format(url[33:])
        file = open(name, 'wb')
        send = requests.get(url)
        file.write(send.content)
        file.close()
