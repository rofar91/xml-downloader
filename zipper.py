import gzip
import os
from get_urls import get_urls


def zipper(path='archives'):
    filenames = []
    for files in os.listdir(path):
        filenames.append(files)     # получаем имена архивов в папке archives

    urls = []
    for files in filenames:
        f = gzip.open(path + '/' + files, 'rb')    # раскрываем каждый архив
        file_content = f.read()
        # print(file_content.decode())
        urls.append(get_urls(file_content.decode()))
        f.close()

    print(len(urls))    # сколько архивов - столько и выведет
