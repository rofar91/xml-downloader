import xml.etree.ElementTree as ET


def get_urls(xml):
    urls = []
    root = ET.fromstring(xml)

    for child in root:
        urls.append(child[0].text)

    return urls



