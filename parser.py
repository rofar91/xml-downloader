import requests
from downloader import download_gz
from get_urls import get_urls
from zipper import zipper

url = 'https://market.yandex.ru/sitemap/sitemap-model_main.xml'
xmls = requests.get(url)

urls = get_urls(xmls.text)  # получаем ссылки на архивы

download_gz(urls)  # скачиваем архивы в каталог archives

zipper()  # распаковываем архивы и извлекаем ссылки из архивов
